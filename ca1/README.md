# Class Assignment 1 Report

Here you should write the report for the assignment.

You should use Markdown Syntax so that the report is properly rendered in Bitbucket. See how to write README files for Bitbucket in [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)

The source code for this assignment is located in the folder [ca1/tut-basic](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1/tut-basic)

As a suggestion, the report should have the following sections.

## 1. Analysis, Design and Implementation

*A section dedicated to the description of the analysis, design and implementation of the requirements*

*Should follow a "tutorial" style (i.e., it should be possible to reproduce the assignment by following the instructions in the tutorial)*

* *Should include a description of the steps used to achieve the requirements*
* *Should include justifications for the options (when required)*

## 2. Analysis of an Alternative

*For this assignment is there an aterntiave tool for Git? Describe it and compare it ti Git*

## 3. Implementation of the Alternative

*Present the implementation of this class assignment using the git alternative*
